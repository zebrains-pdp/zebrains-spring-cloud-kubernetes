package ru.prestu.security.filter;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.DefaultUriBuilderFactory;
import reactor.core.publisher.Mono;
import ru.prestu.constant.ClaimsConstant;
import ru.prestu.constant.WebConstant;
import ru.prestu.exception.UnauthorizedException;
import ru.prestu.jwt.JwtService;
import ru.prestu.model.UserDetails;
import ru.prestu.model.enumeration.Role;

import java.util.UUID;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PROTECTED, makeFinal = true)
public abstract class MicroserviceAuthorizationFilter extends AbstractAuthorizationFilter {

    String authServiceUri;
    JwtService jwtService;
    WebClient.Builder webClientBuilder;

    @Override
    protected Mono<Authentication> buildAuthentication(ServerHttpRequest request) {
        return jwtService.extractTokenFromRequest(request)
                .flatMap(token -> {
                    String validateSessionUrl = new DefaultUriBuilderFactory(authServiceUri)
                            .builder()
                            .path(WebConstant.VALIDATE_TOKEN_PATH)
                            .build().toString();
                    return webClientBuilder.baseUrl(validateSessionUrl).build()
                            .post()
                            .header(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", token))
                            .retrieve()
                            .onStatus(
                                    status -> status.is4xxClientError() || status.is5xxServerError(),
                                    response -> Mono.error(UnauthorizedException::new)
                            )
                            .bodyToMono(Void.class)
                            .thenReturn(token);
                })
                .flatMap(jwtService::extractAllClaims)
                .map(claims -> {
                    UserDetails userDetails = UserDetails.builder()
                            .username(claims.getSubject())
                            .id(UUID.fromString(claims.get(ClaimsConstant.ID_CLAIM_NAME, String.class)))
                            .role(Role.resolve(claims.get(ClaimsConstant.ROLE_CLAIM_NAME, String.class)))
                            .build();
                    return new UsernamePasswordAuthenticationToken(
                            userDetails,
                            null,
                            userDetails.getAuthorities()
                    );
                });
    }
}
