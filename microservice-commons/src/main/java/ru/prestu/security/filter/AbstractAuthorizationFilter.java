package ru.prestu.security.filter;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.util.List;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PROTECTED, makeFinal = true)
public abstract class AbstractAuthorizationFilter implements WebFilter {

    PathMatcher pathMatcher = new AntPathMatcher();

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        return Mono.just(exchange.getRequest())
                .flatMap(request -> {
                    String path = request.getPath().toString();
                    if (getAuthorizationFreePathPatterns().stream()
                            .noneMatch(pathPattern -> pathMatcher.match(pathPattern, path))) {
                        return buildAuthentication(request)
                                .flatMap(authentication -> chain.filter(exchange)
                                        .contextWrite(context ->
                                                ReactiveSecurityContextHolder.withAuthentication(authentication)
                                        )
                                );
                    }
                    return chain.filter(exchange);
                });
    }

    protected abstract Mono<Authentication> buildAuthentication(ServerHttpRequest request);

    protected abstract List<String> getAuthorizationFreePathPatterns();
}
