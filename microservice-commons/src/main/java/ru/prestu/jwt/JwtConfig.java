package ru.prestu.jwt;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.prestu.jwt")
public class JwtConfig {

}
