package ru.prestu.jwt;

import io.jsonwebtoken.Claims;
import org.springframework.http.server.reactive.ServerHttpRequest;
import reactor.core.publisher.Mono;

import java.security.Key;
import java.util.function.Function;

public interface JwtService {

    Mono<String> extractTokenFromRequest(ServerHttpRequest httpRequest);

    Mono<Claims> extractAllClaims(String token);

    <T> Mono<T> extractClaim(String token, Function<Claims, T> claimsResolver);

    Mono<String> extractUsername(String token);

    Mono<Boolean> tokenValid(String token);

    Mono<Key> getSignInKey();
}
