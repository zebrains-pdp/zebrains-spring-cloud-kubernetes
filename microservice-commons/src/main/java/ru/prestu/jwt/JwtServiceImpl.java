package ru.prestu.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.prestu.exception.InvalidTokenException;
import ru.prestu.exception.TokenNotFoundException;

import java.security.Key;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class JwtServiceImpl implements JwtService {

    JwtProperty jwtProperty;

    @Override
    public Mono<String> extractTokenFromRequest(ServerHttpRequest httpRequest) {
        return Mono.just(httpRequest)
                .map(request -> Optional.ofNullable(request.getHeaders().get(HttpHeaders.AUTHORIZATION))
                        .orElse(Collections.emptyList())
                )
                .filter(authHeaders -> authHeaders.size() == 1)
                .map(authHeaders -> authHeaders.get(0))
                .filter(authHeader -> Objects.nonNull(authHeader) && authHeader.startsWith("Bearer "))
                .map(authHeader -> authHeader.substring(7))
                .switchIfEmpty(Mono.error(TokenNotFoundException::new));
    }

    @Override
    public Mono<Claims> extractAllClaims(String token) {
        return getSignInKey()
                .map(key -> {
                    try {
                        return Jwts
                                .parserBuilder()
                                .setSigningKey(key)
                                .build()
                                .parseClaimsJws(token)
                                .getBody();
                    } catch (Exception e) {
                        throw new InvalidTokenException();
                    }
                });
    }

    @Override
    public <T> Mono<T> extractClaim(String token, Function<Claims, T> claimsResolver) {
        return extractAllClaims(token)
                .map(claimsResolver);
    }

    @Override
    public Mono<String> extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    @Override
    public Mono<Boolean> tokenValid(String token) {
        return tokenNotExpired(token);
    }

    @Override
    public Mono<Key> getSignInKey() {
        return Mono.just(Decoders.BASE64.decode(jwtProperty.getSecretKey()))
                .map(Keys::hmacShaKeyFor);
    }

    private Mono<Boolean> tokenNotExpired(String token) {
        return extractClaim(token, Claims::getExpiration)
                .map(expiration -> expiration.after(new Date()));
    }
}
