package ru.prestu.constant;

import lombok.experimental.UtilityClass;

import java.util.List;

@UtilityClass
public class WebConstant {

    public final String VALIDATE_TOKEN_PATH = "/token/validate";

    public final List<String> SWAGGER_PATH_PATTERNS = List.of(
            "/v3/api-docs/**",
            "/swagger-ui/**",
            "/swagger-resources",
            "/swagger-resources/**",
            "/actuator/**",
            "/favicon.ico",
            "/webjars/**",
            "/swagger-ui.html"
    );
}
