package ru.prestu.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ClaimsConstant {

    public final String ID_CLAIM_NAME = "id";
    public final String ROLE_CLAIM_NAME = "role";
}
