package ru.prestu.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SwaggerInfo {

	public final String SECURITY_SCHEME_NAME = "BearerAuth";
}
