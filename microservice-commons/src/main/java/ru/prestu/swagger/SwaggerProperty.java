package ru.prestu.swagger;

public interface SwaggerProperty {

    String getTitle();
    String getVersion();
}
