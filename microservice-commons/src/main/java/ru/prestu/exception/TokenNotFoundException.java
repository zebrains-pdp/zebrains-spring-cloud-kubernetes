package ru.prestu.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class TokenNotFoundException extends HttpStatusCodeException {

    public TokenNotFoundException() {
        super(HttpStatus.BAD_REQUEST, "Token not found");
    }
}
