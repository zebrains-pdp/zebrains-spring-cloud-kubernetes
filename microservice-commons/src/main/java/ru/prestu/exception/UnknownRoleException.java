package ru.prestu.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import ru.prestu.model.enumeration.Role;

public class UnknownRoleException extends HttpStatusCodeException {

    public UnknownRoleException(String role) {
        super(
                HttpStatus.BAD_REQUEST,
                String.format("Unknown role: %s. Available roles: %s", role, Role.getListOfRoles())
        );
    }
}
