package ru.prestu.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class DocumentConstant {

	public final String ID_FIELD_NAME = "_id";
}
