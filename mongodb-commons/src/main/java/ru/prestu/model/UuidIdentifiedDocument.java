package ru.prestu.model;

import java.util.UUID;

public interface UuidIdentifiedDocument {

	UUID getId();

	void setId(UUID id);
}
