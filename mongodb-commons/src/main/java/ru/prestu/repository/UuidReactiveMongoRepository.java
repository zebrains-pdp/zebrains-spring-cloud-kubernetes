package ru.prestu.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.UUID;

@NoRepositoryBean
public interface UuidReactiveMongoRepository<T> extends ReactiveMongoRepository<T, UUID> {

}
