package ru.prestu.repository.impl;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PROTECTED, makeFinal = true)
public class AbstractCustomMongoRepository {

    ReactiveMongoTemplate mongoTemplate;
}
