package ru.prestu.config;

public interface MongoProperty {

    String getDatabaseName();

    String getHost();

    String getPort();

    String getUsername();

    String getPassword();

    default String getUrl() {
        return String.format("mongodb://%s:%s/%s", getHost(), getPort(), getDatabaseName());
    }
}
