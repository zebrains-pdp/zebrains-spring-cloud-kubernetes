package ru.prestu.config;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.bson.UuidRepresentation;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoReactiveDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoReactiveAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.mapping.event.ReactiveBeforeConvertCallback;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import reactor.core.publisher.Mono;
import ru.prestu.model.UuidIdentifiedDocument;

import java.util.Objects;
import java.util.UUID;

@Configuration
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@EnableAutoConfiguration(exclude = {
        MongoReactiveAutoConfiguration.class,
        MongoAutoConfiguration.class,
        MongoReactiveDataAutoConfiguration.class,
        MongoDataAutoConfiguration.class
})
public class MongoConfig extends AbstractReactiveMongoConfiguration {

    MongoProperty mongoProperty;

    @Override
    protected String getDatabaseName() {
        return mongoProperty.getDatabaseName();
    }

    @Bean
    public MongoClient reactiveMongoClient() {
        System.out.println();
        ConnectionString connectionString = new ConnectionString(mongoProperty.getUrl());
        MongoCredential credential = MongoCredential.createCredential(
                mongoProperty.getUsername(),
                mongoProperty.getDatabaseName(),
                mongoProperty.getPassword().toCharArray()
        );
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .uuidRepresentation(UuidRepresentation.STANDARD)
                .applyConnectionString(connectionString)
                .credential(credential)
                .build();
        return MongoClients.create(mongoClientSettings);
    }

    @Bean
    public ReactiveMongoTemplate mongoTemplate(MongoClient mongoClient) {
        return new ReactiveMongoTemplate(mongoClient, getDatabaseName());
    }

    @Bean
    public ReactiveBeforeConvertCallback<UuidIdentifiedDocument> beforeConvertCallback() {
        return (document, collection) -> {
            if (Objects.isNull(document.getId())) {
                document.setId(UUID.randomUUID());
            }
            return Mono.just(document);
        };
    }

    @Bean
    public ValidatingMongoEventListener validatingMongoEventListener(LocalValidatorFactoryBean localValidatorFactoryBean) {
        return new ValidatingMongoEventListener(localValidatorFactoryBean);
    }
}
