package ru.prestu.auth.config;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.reactive.WebClientCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import ru.prestu.auth.config.property.JwtProperty;
import ru.prestu.auth.config.property.MongoProperty;
import ru.prestu.auth.config.property.SwaggerProperty;
import ru.prestu.auth.mapper.UserDetailsMapper;
import ru.prestu.auth.repository.UserRepository;
import ru.prestu.common.CommonConfig;
import ru.prestu.config.MongoConfig;
import ru.prestu.exception.UnauthorizedException;
import ru.prestu.jwt.JwtConfig;
import ru.prestu.swagger.SwaggerConfig;


@Configuration
@RequiredArgsConstructor
@Import({
        CommonConfig.class,
        JwtConfig.class,
        SwaggerConfig.class,
        MongoConfig.class
})
@EnableConfigurationProperties({
        JwtProperty.class,
        MongoProperty.class,
        SwaggerProperty.class
})
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@EnableReactiveMongoRepositories(basePackages = "ru.prestu.auth.repository")
public class ApplicationConfig {

    UserRepository userRepository;
    UserDetailsMapper userDetailsMapper;

    @Bean
    public ReactiveUserDetailsService reactiveUserDetailsService() {
        return username -> userRepository.findByUsername(username)
                .map(userDetailsMapper::toUserDetails);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    protected ReactiveAuthenticationManager reactiveAuthenticationManager(
            ReactiveUserDetailsService reactiveUserDetailsService,
            PasswordEncoder passwordEncoder
    ) {
        return authentication -> {
            String username = authentication.getPrincipal().toString();
            CharSequence password = authentication.getCredentials().toString();
            return reactiveUserDetailsService.findByUsername(username)
                    .filter(user -> passwordEncoder.matches(password, user.getPassword()))
                    .switchIfEmpty(Mono.error(UnauthorizedException::new))
                    .map(user -> new UsernamePasswordAuthenticationToken(
                            user,
                            user.getPassword(),
                            user.getAuthorities()
                    ));
        };
    }

    @Bean
    @LoadBalanced
    public WebClient.Builder webClientBuilder(ObjectProvider<WebClientCustomizer> customizerProvider) {
        WebClient.Builder builder = WebClient.builder();
        customizerProvider.orderedStream().forEach(customizer -> customizer.customize(builder));
        return builder;
    }
}
