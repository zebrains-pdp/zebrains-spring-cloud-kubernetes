package ru.prestu.auth.config.property;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@FieldDefaults(level = AccessLevel.PRIVATE)
@ConfigurationProperties(prefix = "jwt")
public class JwtProperty implements ru.prestu.jwt.JwtProperty {

    @NotBlank
    String secretKey;
    @NotNull
    TokenProperty accessToken;
    @NotNull
    TokenProperty refreshToken;

    @Data
    @Validated
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class TokenProperty {

        @NotNull
        Long expiration;
    }
}
