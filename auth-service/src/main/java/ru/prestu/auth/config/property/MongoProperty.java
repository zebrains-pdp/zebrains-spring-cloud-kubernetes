package ru.prestu.auth.config.property;

import jakarta.validation.constraints.NotBlank;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@ConfigurationProperties(prefix = "mongodb")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MongoProperty implements ru.prestu.config.MongoProperty {

    @NotBlank
    String databaseName;
    @NotBlank
    String host;
    @NotBlank
    String port;
    @NotBlank
    String username;
    @NotBlank
    String password;
}
