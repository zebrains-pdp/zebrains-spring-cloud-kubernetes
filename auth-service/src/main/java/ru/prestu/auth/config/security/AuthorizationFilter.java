package ru.prestu.auth.config.security;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;
import ru.prestu.auth.constant.web.AuthControllerConstant;
import ru.prestu.auth.repository.TokenRepository;
import ru.prestu.constant.WebConstant;
import ru.prestu.exception.UnauthorizedException;
import ru.prestu.jwt.JwtService;
import ru.prestu.security.filter.AbstractAuthorizationFilter;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AuthorizationFilter extends AbstractAuthorizationFilter {

    JwtService jwtService;
    TokenRepository tokenRepository;
    ReactiveUserDetailsService reactiveUserDetailsService;

    @Override
    protected List<String> getAuthorizationFreePathPatterns() {
        return new ArrayList<>() {{
            addAll(WebConstant.SWAGGER_PATH_PATTERNS);
            add(String.format("%s%s", AuthControllerConstant.ROOT_PATH, AuthControllerConstant.REGISTER_PATH));
            add(String.format("%s%s", AuthControllerConstant.ROOT_PATH, AuthControllerConstant.LOGIN_PATH));
        }};
    }

    @Override
    protected Mono<Authentication> buildAuthentication(ServerHttpRequest request) {
        return jwtService.extractTokenFromRequest(request)
                .flatMap(token -> Mono.zip(
                        Mono.just(token),
                        jwtService.extractUsername(token),
                        Tuples::of
                ))
                .flatMap(tokenWithUsername -> {
                    String token = tokenWithUsername.getT1();
                    String username = tokenWithUsername.getT2();
                    return Mono.zip(
                            reactiveUserDetailsService.findByUsername(username),
                            tokenRepository.findByAccessToken(token),
                            Tuples::of
                    );
                })
                .switchIfEmpty(Mono.error(UnauthorizedException::new))
                .flatMap(userDetailsWithToken -> {
                    UserDetails userDetails = userDetailsWithToken.getT1();
                    String token = userDetailsWithToken.getT2().getAccessToken();
                    return jwtService.tokenValid(token)
                            .filter(valid -> valid)
                            .map(tokenValid -> new UsernamePasswordAuthenticationToken(
                                    userDetails,
                                    null,
                                    userDetails.getAuthorities()
                            ));
                });
    }
}
