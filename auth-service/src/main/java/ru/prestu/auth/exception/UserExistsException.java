package ru.prestu.auth.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class UserExistsException extends HttpStatusCodeException {

    public UserExistsException(String username) {
        super(HttpStatus.BAD_REQUEST, String.format("User exists: username='%s'", username));
    }
}
