package ru.prestu.auth.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class UserNotFoundException extends HttpStatusCodeException {

    public UserNotFoundException(String username) {
        super(HttpStatus.NOT_FOUND, String.format("User not found: username='%s'", username));
    }
}
