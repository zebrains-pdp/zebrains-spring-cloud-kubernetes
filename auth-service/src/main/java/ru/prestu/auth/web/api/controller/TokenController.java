package ru.prestu.auth.web.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.prestu.constant.SwaggerInfo;
import ru.prestu.auth.constant.web.TokenControllerConstant;

@Tag(
        name = TokenControllerConstant.TAG_NAME,
        description = TokenControllerConstant.TAG_DESCRIPTION
)
@RestController
@RequestMapping(TokenControllerConstant.ROOT_PATH)
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TokenController {

    @PostMapping(TokenControllerConstant.VALIDATE_PATH)
    @Operation(
            summary = "Token validation",
            security = @SecurityRequirement(name = SwaggerInfo.SECURITY_SCHEME_NAME),
            responses = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Токен валидный"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "Токен не валидный",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ErrorResponse.class)
                            )
                    )
            }
    )
    public Mono<ResponseEntity<Void>> validate() {
        return Mono.just(ResponseEntity.noContent().build());
    }
}
