package ru.prestu.auth.web.api.facade;

import reactor.core.publisher.Mono;
import ru.prestu.auth.web.api.response.LoginResponse;
import ru.prestu.auth.web.api.request.LoginRequest;
import ru.prestu.auth.web.api.request.RegisterRequest;
import ru.prestu.auth.web.api.response.RegisterResponse;

public interface AuthFacade {

    Mono<RegisterResponse> register(RegisterRequest registerRequest);

    Mono<LoginResponse> login(LoginRequest loginRequest);
}
