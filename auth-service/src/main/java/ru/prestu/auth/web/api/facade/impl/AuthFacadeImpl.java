package ru.prestu.auth.web.api.facade.impl;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.prestu.auth.mapper.UserMapper;
import ru.prestu.auth.service.AuthService;
import ru.prestu.auth.service.UserService;
import ru.prestu.auth.web.api.request.LoginRequest;
import ru.prestu.auth.web.api.facade.AuthFacade;
import ru.prestu.auth.web.api.request.RegisterRequest;
import ru.prestu.auth.web.api.response.LoginResponse;
import ru.prestu.auth.web.api.response.RegisterResponse;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AuthFacadeImpl implements AuthFacade {

    AuthService authService;
    UserService userService;
    UserMapper userMapper;

    @Override
    public Mono<RegisterResponse> register(RegisterRequest registerRequest) {
        return Mono.just(registerRequest)
                .map(userMapper::toDocument)
                .flatMap(userService::createUser)
                .map(userMapper::toRegisterResponse);
    }

    @Override
    public Mono<LoginResponse> login(LoginRequest loginRequest) {
        return authService.login(loginRequest);
    }
}
