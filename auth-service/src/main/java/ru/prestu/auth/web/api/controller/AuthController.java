package ru.prestu.auth.web.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.prestu.constant.SwaggerInfo;
import ru.prestu.auth.constant.web.AuthControllerConstant;
import ru.prestu.auth.web.api.facade.AuthFacade;
import ru.prestu.auth.web.api.request.LoginRequest;
import ru.prestu.auth.web.api.request.RegisterRequest;
import ru.prestu.auth.web.api.response.LoginResponse;
import ru.prestu.auth.web.api.response.RegisterResponse;

@Tag(
        name = AuthControllerConstant.TAG_NAME,
        description = AuthControllerConstant.TAG_DESCRIPTION
)
@RestController
@RequestMapping(AuthControllerConstant.ROOT_PATH)
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AuthController {

    AuthFacade authFacade;

    @PostMapping(AuthControllerConstant.REGISTER_PATH)
    @Operation(
            summary = "Registration",
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "User data",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = RegisterRequest.class)
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "User data",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = RegisterResponse.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Invalid request data",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ErrorResponse.class)
                            )
                    )
            }
    )
    public Mono<ResponseEntity<RegisterResponse>> register(@RequestBody @Validated RegisterRequest registerRequest) {
        return authFacade.register(registerRequest)
                .map(response -> ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(response)
                );
    }

    @PostMapping(AuthControllerConstant.LOGIN_PATH)
    @Operation(
            summary = "Login",
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "Credentials",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = LoginRequest.class)
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Tokens",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = LoginResponse.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Invalid credentials",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ErrorResponse.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "Unauthorized",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ErrorResponse.class)
                            )
                    )
            }
    )
    public Mono<ResponseEntity<LoginResponse>> login(@RequestBody @Validated LoginRequest loginRequest) {
        return authFacade.login(loginRequest)
                .map(ResponseEntity::ok);
    }

    @PostMapping(AuthControllerConstant.LOGOUT_PATH)
    @Operation(
            summary = "Logout",
            security = @SecurityRequirement(name = SwaggerInfo.SECURITY_SCHEME_NAME),
            responses = @ApiResponse(responseCode = "204")
    )
    public Mono<ResponseEntity<Void>> logout() {
        throw new UnsupportedOperationException("Implemented by Spring Security");
    }
}
