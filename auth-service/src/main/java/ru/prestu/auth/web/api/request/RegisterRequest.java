package ru.prestu.auth.web.api.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import ru.prestu.model.enumeration.Role;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterRequest {

    @NotBlank(message = "Username must be not blank")
    String username;
    @NotBlank(message = "Password must be not blank")
    @Size(min = 8, max = 20, message = "Password must be between 8 and 20 characters long")
    String password;
    @NotNull(message = "Country code must be not blanc")
    @Pattern(regexp = "\\d{3}", message = "Country code must consist of 3 digits")
    String countryCode;
    @NotNull(message = "Role must be not blank")
    Role role;
}
