package ru.prestu.auth.model.db;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import ru.prestu.auth.constant.db.UserConstant;
import ru.prestu.model.enumeration.Role;
import ru.prestu.model.UuidIdentifiedDocument;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Document(UserConstant.COLLECTION_NAME)
public class UserDocument implements UuidIdentifiedDocument {

    @Id
    UUID id;
    @Field(UserConstant.USERNAME_FIELD_NAME)
    String username;
    @Field(UserConstant.PASSWORD_FIELD_NAME)
    String password;
    @Field(UserConstant.ROLE_FIELD_NAME)
    Role role;
    @Field(UserConstant.COUNTRY_CODE_FIELD_NAME)
    String countryCode;
}
