package ru.prestu.auth.model.db;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;
import org.springframework.data.mongodb.core.mapping.Field;
import ru.prestu.auth.constant.db.TokenConstant;
import ru.prestu.model.UuidIdentifiedDocument;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Document(TokenConstant.COLLECTION_NAME)
public class TokenDocument implements UuidIdentifiedDocument {

    @Id
    UUID id;
    @Field(TokenConstant.ACCESS_TOKEN_FIELD_NAME)
    String accessToken;
    @Field(TokenConstant.REFRESH_TOKEN_FIELD_NAME)
    String refreshToken;
    @DocumentReference
    @Field(TokenConstant.USER_ID_FIELD_NAME)
    UserDocument user;
}
