package ru.prestu.auth.mapper;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.prestu.auth.mapper.annotation.PasswordMapping;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class PasswordMapper {

    PasswordEncoder passwordEncoder;

    @PasswordMapping
    public String encode(String secret) {
        return passwordEncoder.encode(secret);
    }
}
