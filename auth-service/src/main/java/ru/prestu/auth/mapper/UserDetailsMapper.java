package ru.prestu.auth.mapper;

import org.mapstruct.Mapper;
import ru.prestu.model.UserDetails;
import ru.prestu.auth.model.db.UserDocument;

@Mapper
public interface UserDetailsMapper {

    UserDetails toUserDetails(UserDocument user);
}
