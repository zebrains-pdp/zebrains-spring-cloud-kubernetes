package ru.prestu.auth.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.prestu.auth.mapper.annotation.PasswordMapping;
import ru.prestu.auth.model.db.UserDocument;
import ru.prestu.auth.web.api.request.RegisterRequest;
import ru.prestu.auth.web.api.response.RegisterResponse;

@Mapper(uses = PasswordMapper.class)
public interface UserMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "password", source = "password", qualifiedBy = PasswordMapping.class)
    UserDocument toDocument(RegisterRequest registerRequest);

    RegisterResponse toRegisterResponse(UserDocument user);
}
