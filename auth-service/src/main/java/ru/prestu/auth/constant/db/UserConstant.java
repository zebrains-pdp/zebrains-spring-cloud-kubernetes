package ru.prestu.auth.constant.db;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UserConstant {

    public final String COLLECTION_NAME = "users";

    public final String USERNAME_FIELD_NAME = "username";
    public final String PASSWORD_FIELD_NAME = "password";
    public final String ROLE_FIELD_NAME = "role";
    public final String COUNTRY_CODE_FIELD_NAME = "countryCode";
}
