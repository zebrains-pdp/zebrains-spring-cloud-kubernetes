package ru.prestu.auth.constant.web;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AuthControllerConstant {

    public final String TAG_NAME = "Auth";
    public final String TAG_DESCRIPTION = "Authentication";
    public final String ROOT_PATH = "/auth";

    public final String REGISTER_PATH = "/register";
    public final String LOGIN_PATH = "/login";
    public final String LOGOUT_PATH = "/logout";
}
