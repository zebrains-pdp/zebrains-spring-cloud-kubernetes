package ru.prestu.auth.constant.web;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TokenControllerConstant {

    public final String TAG_NAME = "Token";
    public final String TAG_DESCRIPTION = "Token endpoints";
    public final String ROOT_PATH = "/token";

    public final String VALIDATE_PATH = "/validate";
}
