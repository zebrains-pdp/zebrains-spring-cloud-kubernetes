package ru.prestu.auth.constant.db;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TokenConstant {

    public final String COLLECTION_NAME = "tokens";
    public final String ACCESS_TOKEN_FIELD_NAME = "accessToken";
    public final String REFRESH_TOKEN_FIELD_NAME = "refreshToken";
    public final String USER_ID_FIELD_NAME = "userId";
}
