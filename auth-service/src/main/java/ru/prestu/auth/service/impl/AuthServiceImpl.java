package ru.prestu.auth.service.impl;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.prestu.auth.model.db.TokenDocument;
import ru.prestu.auth.model.db.UserDocument;
import ru.prestu.auth.service.AuthService;
import ru.prestu.auth.service.TokenGenerator;
import ru.prestu.auth.service.TokenService;
import ru.prestu.auth.web.api.request.LoginRequest;
import ru.prestu.auth.web.api.response.LoginResponse;
import ru.prestu.jwt.JwtService;
import ru.prestu.model.UserDetails;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AuthServiceImpl implements AuthService {

    JwtService jwtService;
    TokenService tokenService;
    TokenGenerator tokenGenerator;
    ReactiveAuthenticationManager reactiveAuthenticationManager;

    @Override
    public Mono<LoginResponse> login(LoginRequest loginRequest) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(),
                loginRequest.getPassword()
        );
        return reactiveAuthenticationManager.authenticate(authenticationToken)
                .flatMap(authentication -> {
                    UserDetails userDetails = (UserDetails) authentication.getPrincipal();
                    return Mono.zip(
                            Mono.just(userDetails),
                            tokenGenerator.generateAccessToken(userDetails),
                            tokenGenerator.generateRefreshToken(userDetails)
                    );
                })
                .flatMap(userDetailsWithToken -> tokenService.createToken(
                                        TokenDocument.builder()
                                                .user(UserDocument.builder().id(userDetailsWithToken.getT1().getId()).build())
                                                .accessToken(userDetailsWithToken.getT2())
                                                .refreshToken(userDetailsWithToken.getT3())
                                                .build()
                                )
                                .map(token ->
                                        LoginResponse.builder()
                                                .accessToken(token.getAccessToken())
                                                .refreshToken(token.getRefreshToken())
                                                .build()
                                )
                );
    }

    @Override
    public Mono<Void> logout(WebFilterExchange exchange, Authentication authentication) {
        return Mono.just(exchange.getExchange().getRequest())
                .flatMap(jwtService::extractTokenFromRequest)
                .flatMap(tokenService::deleteByAccessToken);
    }
}
