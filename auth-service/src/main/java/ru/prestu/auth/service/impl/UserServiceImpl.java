package ru.prestu.auth.service.impl;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.prestu.auth.exception.UserExistsException;
import ru.prestu.auth.model.db.UserDocument;
import ru.prestu.auth.repository.UserRepository;
import ru.prestu.auth.service.UserService;

import java.util.Objects;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UserServiceImpl implements UserService {

    UserRepository userRepository;

    @Override
    public Mono<UserDocument> createUser(UserDocument user) {
        String username = user.getUsername();
        return userRepository.findByUsername(username)
                .switchIfEmpty(Mono.just(user))
                .filter(userToCreate -> Objects.isNull(userToCreate.getId()))
                .flatMap(userRepository::save)
                .switchIfEmpty(Mono.error(new UserExistsException(username)));
    }
}
