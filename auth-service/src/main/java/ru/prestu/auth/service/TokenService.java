package ru.prestu.auth.service;

import reactor.core.publisher.Mono;
import ru.prestu.auth.model.db.TokenDocument;

public interface TokenService {

    Mono<TokenDocument> createToken(TokenDocument token);

    Mono<Void> deleteByAccessToken(String accessToken);
}
