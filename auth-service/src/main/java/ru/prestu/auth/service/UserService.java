package ru.prestu.auth.service;

import reactor.core.publisher.Mono;
import ru.prestu.auth.model.db.UserDocument;

public interface UserService {

    Mono<UserDocument> createUser(UserDocument user);
}
