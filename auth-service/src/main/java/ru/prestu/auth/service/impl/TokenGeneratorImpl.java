package ru.prestu.auth.service.impl;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.prestu.auth.config.property.JwtProperty;
import ru.prestu.auth.service.TokenGenerator;
import ru.prestu.constant.ClaimsConstant;
import ru.prestu.jwt.JwtService;
import ru.prestu.model.UserDetails;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TokenGeneratorImpl implements TokenGenerator {

    JwtService jwtService;
    JwtProperty jwtProperty;

    @Override
    public Mono<String> generateAccessToken(UserDetails userDetails) {
        return Mono.just(Map.of(
                ClaimsConstant.ID_CLAIM_NAME, userDetails.getId().toString(),
                ClaimsConstant.ROLE_CLAIM_NAME, userDetails.getRole().name()
        )).flatMap(extraClaims -> buildToken(extraClaims, userDetails, jwtProperty.getAccessToken().getExpiration()));
    }

    @Override
    public Mono<String> generateRefreshToken(UserDetails userDetails) {
        return buildToken(new HashMap<>(), userDetails, jwtProperty.getRefreshToken().getExpiration());
    }

    private Mono<String> buildToken(
            Map<String, ?> extraClaims,
            UserDetails userDetails,
            Long expiration
    ) {
        return jwtService.getSignInKey()
                .map(key -> Jwts
                        .builder()
                        .setClaims(extraClaims)
                        .setSubject(userDetails.getUsername())
                        .setIssuedAt(new Date(System.currentTimeMillis()))
                        .setExpiration(new Date(System.currentTimeMillis() + expiration))
                        .signWith(key, SignatureAlgorithm.HS256)
                        .compact()
                );
    }
}
