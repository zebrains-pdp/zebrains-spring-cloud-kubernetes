package ru.prestu.auth.service;

import org.springframework.security.web.server.authentication.logout.ServerLogoutHandler;
import reactor.core.publisher.Mono;
import ru.prestu.auth.web.api.request.LoginRequest;
import ru.prestu.auth.web.api.response.LoginResponse;

public interface AuthService extends ServerLogoutHandler {

    Mono<LoginResponse> login(LoginRequest loginRequest);
}
