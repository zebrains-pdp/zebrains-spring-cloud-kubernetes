package ru.prestu.auth.service;

import reactor.core.publisher.Mono;
import ru.prestu.model.UserDetails;

public interface TokenGenerator {

    Mono<String> generateAccessToken(UserDetails userDetails);

    Mono<String> generateRefreshToken(UserDetails userDetails);
}
