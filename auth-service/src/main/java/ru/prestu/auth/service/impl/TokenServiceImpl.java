package ru.prestu.auth.service.impl;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.prestu.auth.model.db.TokenDocument;
import ru.prestu.auth.repository.TokenRepository;
import ru.prestu.auth.service.TokenService;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TokenServiceImpl implements TokenService {

    TokenRepository tokenRepository;

    @Override
    public Mono<TokenDocument> createToken(TokenDocument token) {
        return tokenRepository.save(token);
    }

    @Override
    public Mono<Void> deleteByAccessToken(String accessToken) {
        return tokenRepository.deleteByAccessToken(accessToken);
    }
}
