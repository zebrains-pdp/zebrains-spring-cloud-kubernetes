package ru.prestu.auth.repository;

import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.prestu.auth.model.db.TokenDocument;
import ru.prestu.repository.UuidReactiveMongoRepository;

@Repository
public interface TokenRepository extends UuidReactiveMongoRepository<TokenDocument> {

    Mono<TokenDocument> findByAccessToken(String accessToken);

    Mono<Void> deleteByAccessToken(String accessToken);
}
