package ru.prestu.auth.repository;

import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.prestu.auth.model.db.UserDocument;
import ru.prestu.repository.UuidReactiveMongoRepository;

@Repository
public interface UserRepository extends UuidReactiveMongoRepository<UserDocument> {

    Mono<UserDocument> findByUsername(String username);
}
