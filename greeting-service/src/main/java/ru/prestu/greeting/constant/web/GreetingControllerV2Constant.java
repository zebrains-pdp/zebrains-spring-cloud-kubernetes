package ru.prestu.greeting.constant.web;

import lombok.experimental.UtilityClass;

@UtilityClass
public class GreetingControllerV2Constant {

    public final String GREETING_CONTROLLER_V2_TAG_NAME = "Greeting";
    public final String GREETING_CONTROLLER_V2_TAG_DESCRIPTION = "User greeting";
    public static final String GREETING_CONTROLLER_V2_ROOT_PATH = "/v2/greeting";
}
