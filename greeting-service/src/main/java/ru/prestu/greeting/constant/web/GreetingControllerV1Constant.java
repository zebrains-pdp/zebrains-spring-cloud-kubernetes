package ru.prestu.greeting.constant.web;

import lombok.experimental.UtilityClass;

@UtilityClass
public class GreetingControllerV1Constant {

	public final String GREETING_CONTROLLER_V1_TAG_NAME = "Greeting";
	public final String GREETING_CONTROLLER_V1_TAG_DESCRIPTION = "User greeting";
	public static final String GREETING_CONTROLLER_V1_ROOT_PATH = "/v1/greeting";
}
