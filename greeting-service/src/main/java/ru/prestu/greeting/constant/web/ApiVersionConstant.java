package ru.prestu.greeting.constant.web;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ApiVersionConstant {

    public final String V1_ROOT_PATH = "/v1";
    public final String V2_ROOT_PATH = "/v2";
}
