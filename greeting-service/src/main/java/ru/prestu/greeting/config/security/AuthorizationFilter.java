package ru.prestu.greeting.config.security;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import ru.prestu.constant.WebConstant;
import ru.prestu.greeting.constant.web.ApiVersionConstant;
import ru.prestu.jwt.JwtService;
import ru.prestu.security.filter.MicroserviceAuthorizationFilter;

import java.util.ArrayList;
import java.util.List;

@Component
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AuthorizationFilter extends MicroserviceAuthorizationFilter {

    public AuthorizationFilter(
            JwtService jwtService,
            WebClient.Builder webClientBuilder,
            @Value("${auth-service-uri}") String authServiceUri
    ) {
        super(authServiceUri, jwtService, webClientBuilder);
    }

    @Override
    protected List<String> getAuthorizationFreePathPatterns() {
        return new ArrayList<>() {{
            addAll(WebConstant.SWAGGER_PATH_PATTERNS);
            add(String.format("%s/**", ApiVersionConstant.V1_ROOT_PATH));
        }};
    }
}
