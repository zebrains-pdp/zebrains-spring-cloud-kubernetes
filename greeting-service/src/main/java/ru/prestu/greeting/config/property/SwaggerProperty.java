package ru.prestu.greeting.config.property;

import jakarta.validation.constraints.NotBlank;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@RefreshScope
@FieldDefaults(level = AccessLevel.PRIVATE)
@ConfigurationProperties(prefix = "swagger")
public class SwaggerProperty implements ru.prestu.swagger.SwaggerProperty {

    @NotBlank
    String title;
    @NotBlank
    String version;
}
