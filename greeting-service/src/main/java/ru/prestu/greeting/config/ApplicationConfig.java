package ru.prestu.greeting.config;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.reactive.WebClientCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.function.client.WebClient;
import ru.prestu.common.CommonConfig;
import ru.prestu.greeting.config.property.JwtProperty;
import ru.prestu.greeting.config.property.SwaggerProperty;
import ru.prestu.jwt.JwtConfig;
import ru.prestu.security.SecurityConfig;
import ru.prestu.swagger.SwaggerConfig;


@Configuration
@RequiredArgsConstructor
@Import({
        CommonConfig.class,
        JwtConfig.class,
        SecurityConfig.class,
        SwaggerConfig.class
})
@EnableConfigurationProperties({
        JwtProperty.class,
        SwaggerProperty.class
})
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ApplicationConfig {

    @Bean
    @LoadBalanced
    public WebClient.Builder webClientBuilder(ObjectProvider<WebClientCustomizer> customizerProvider) {
        WebClient.Builder builder = WebClient.builder();
        customizerProvider.orderedStream().forEach(customizer -> customizer.customize(builder));
        return builder;
    }
}
