package ru.prestu.greeting.web.api.v2.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.prestu.constant.SwaggerInfo;
import ru.prestu.greeting.constant.web.GreetingControllerV2Constant;
import ru.prestu.greeting.web.api.facade.GreetingFacade;

@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping(GreetingControllerV2Constant.GREETING_CONTROLLER_V2_ROOT_PATH)
@Tag(
        name = GreetingControllerV2Constant.GREETING_CONTROLLER_V2_TAG_NAME,
        description = GreetingControllerV2Constant.GREETING_CONTROLLER_V2_TAG_DESCRIPTION
)
public class GreetingControllerV2 {

    GreetingFacade greetingFacade;

    @GetMapping
    @Operation(
            summary = "Greeting",
            security = @SecurityRequirement(name = SwaggerInfo.SECURITY_SCHEME_NAME),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Greet user",
                            content = @Content(
                                    mediaType = MediaType.TEXT_PLAIN_VALUE,
                                    schema = @Schema(implementation = String.class)
                            )
                    )
            }
    )
    public Mono<ResponseEntity<String>> greetWithUsername() {
        return greetingFacade.greetWithUsername()
                .map(ResponseEntity::ok);
    }
}
