package ru.prestu.greeting.web.api.v1.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.prestu.constant.SwaggerInfo;
import ru.prestu.greeting.constant.web.GreetingControllerV1Constant;
import ru.prestu.greeting.web.api.facade.GreetingFacade;

@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping(GreetingControllerV1Constant.GREETING_CONTROLLER_V1_ROOT_PATH)
@Tag(
        name = GreetingControllerV1Constant.GREETING_CONTROLLER_V1_TAG_NAME,
        description = GreetingControllerV1Constant.GREETING_CONTROLLER_V1_TAG_DESCRIPTION
)
public class GreetingControllerV1 {

    GreetingFacade greetingFacade;

    @GetMapping
    @Operation(
            summary = "Greeting",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Greet user",
                            content = @Content(
                                    mediaType = MediaType.TEXT_PLAIN_VALUE,
                                    schema = @Schema(implementation = String.class)
                            )
                    )
            }
    )
    public Mono<ResponseEntity<String>> greet() {
        return greetingFacade.greet()
                .map(ResponseEntity::ok);
    }
}
