package ru.prestu.greeting.web.api.facade;

import reactor.core.publisher.Mono;

public interface GreetingFacade {

    Mono<String> greet();

    Mono<String> greetWithUsername();
}
