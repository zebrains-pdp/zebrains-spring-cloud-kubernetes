package ru.prestu.greeting.web.api.facade;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import ru.prestu.model.UserDetails;

import static org.springframework.security.web.server.context.WebSessionServerSecurityContextRepository.DEFAULT_SPRING_SECURITY_CONTEXT_ATTR_NAME;

@Service
@RefreshScope
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class GreetingFacadeImpl implements GreetingFacade {

    String greetingTarget;

    public GreetingFacadeImpl(@Value("${greeting.target}") String greetingTarget) {
        this.greetingTarget = greetingTarget;
    }

    @Override
    public Mono<String> greet() {
        return Mono.just(greetingTarget)
                .filter(StringUtils::isNotBlank)
                .map(greetingTarget -> String.format("Hello, %s", greetingTarget))
                .defaultIfEmpty("Hello");
    }

    @Override
    public Mono<String> greetWithUsername() {
        return ReactiveSecurityContextHolder.getContext()
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getPrincipal)
                .cast(UserDetails.class)
                .map(UserDetails::getUsername)
                .map(username -> String.format("Hello, %s", username))
                .defaultIfEmpty("Hello");
    }
}
