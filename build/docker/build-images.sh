#!/bin/bash

function build_lib() {
  LIB_NAME=$1

  cd $1
  mvn clean install -DskipTests
  cd ../
}

function build_image() {
  APP_NAME=$1

  cd $1
  mvn clean package spring-boot:build-image -Pkubernetes -DskipTests
  docker tag $1:0.0.1 $1:2.0.0
  cd ../
}

cd ../../
echo "Building libs"
build_lib mongodb-commons
build_lib microservice-commons
echo "Building Docker images"
build_image auth-service
build_image greeting-service
build_image api-gateway
cd build/docker/
