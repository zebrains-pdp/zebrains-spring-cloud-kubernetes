#!/bin/bash

echo "Destroy mongodb"
kubectl delete -f mongodb-nodeport-svc.yaml
kubectl delete -f mongodb-deployment.yaml
kubectl delete -f mongodb-pvc.yaml
kubectl delete -f mongodb-secrets.yaml
