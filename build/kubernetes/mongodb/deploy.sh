#!/bin/bash

echo "Deploy mongodb"
kubectl apply -f mongodb-secrets.yaml
kubectl create -f mongodb-pvc.yaml
kubectl apply -f mongodb-deployment.yaml
kubectl create -f mongodb-nodeport-svc.yaml
