#!/bin/bash

echo "Destroy namespace-reader"
kubectl delete -f namespace-reader.yml
echo "Destroy config maps"
kubectl delete -f configmap.yml
echo "Destroy secret maps"
kubectl delete -f secret.yml