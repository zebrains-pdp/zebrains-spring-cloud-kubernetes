#!/bin/bash

echo "Deploy namespace-reader"
kubectl apply -f namespace-reader.yml
echo "Deploy config maps"
kubectl apply -f configmap.yml
echo "Deploy secret maps"
kubectl apply -f secret.yml