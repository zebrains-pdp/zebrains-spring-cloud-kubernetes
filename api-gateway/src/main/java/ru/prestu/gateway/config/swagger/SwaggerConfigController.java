package ru.prestu.gateway.config.swagger;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.prestu.gateway.util.RouteDefinitionUtil;

import java.util.ArrayList;
import java.util.List;

@Hidden
@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SwaggerConfigController {

	RouteDefinitionLocator routeDefinitionLocator;

	@GetMapping("/swagger-config.json")
	public SwaggerUrlsConfig swaggerConfig() {
		List<SwaggerUrlsConfig.SwaggerUrl> urls = new ArrayList<>();
		routeDefinitionLocator.getRouteDefinitions().subscribe(routeDefinition -> {
			String name = routeDefinition.getId();
			String path = RouteDefinitionUtil.getApiDocsPath(routeDefinition);
			urls.add(SwaggerUrlsConfig.SwaggerUrl.builder()
					.url(path)
					.name(name)
					.build()
			);
		});
		return new SwaggerUrlsConfig(urls);
	}

	@Data
	@AllArgsConstructor
	@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
	public static class SwaggerUrlsConfig {

		List<SwaggerUrl> urls;

		@Data
		@Builder
		@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
		public static class SwaggerUrl {

			String url;
			String name;
		}
	}
}
