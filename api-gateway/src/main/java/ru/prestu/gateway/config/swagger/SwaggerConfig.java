package ru.prestu.gateway.config.swagger;

import io.swagger.v3.oas.models.servers.Server;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.factory.rewrite.RewriteFunction;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.Buildable;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;
import ru.prestu.gateway.config.property.ServiceProperty;
import ru.prestu.gateway.constant.BeanNames;
import ru.prestu.gateway.constant.OpenApiConstant;
import ru.prestu.gateway.util.RouteDefinitionUtil;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

@Configuration
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SwaggerConfig {

    String applicationUrl;
    Map<String, ServiceProperty> idToServicePropertiesMap;
    RouteDefinitionLocator routeDefinitionLocator;

    public SwaggerConfig(
            @Value("${spring.application.url}") String applicationUrl,
            @Qualifier(BeanNames.ID_TO_SERVICE_PROPERTIES_MAP)
            Map<String, ServiceProperty> idToServicePropertiesMap,
            RouteDefinitionLocator routeDefinitionLocator
    ) {
        this.applicationUrl = applicationUrl;
        this.idToServicePropertiesMap = idToServicePropertiesMap;
        this.routeDefinitionLocator = routeDefinitionLocator;
    }

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        RouteLocatorBuilder.Builder routes = builder.routes();
        routeDefinitionLocator.getRouteDefinitions().subscribe(routeDefinition ->
                routes.route(routeForApiDocs(routeDefinition))
        );
        return routes.build();
    }

    private Function<PredicateSpec, Buildable<Route>> routeForApiDocs(RouteDefinition routeDefinition) {
        return r -> r.path(RouteDefinitionUtil.getApiDocsPath(routeDefinition))
                .filters(filter -> filter
                        .setPath(idToServicePropertiesMap.get(routeDefinition.getId()).getApiPath())
                        .modifyResponseBody(Map.class, Map.class, modifyOpenApi(routeDefinition))
                ).uri(routeDefinition.getUri());
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private RewriteFunction<Map, Map> modifyOpenApi(RouteDefinition routeDefinition) {
        return (serverWebExchange, openApi) -> {
            Server server = new Server();
            server.setUrl(applicationUrl + RouteDefinitionUtil.getServicePath(routeDefinition));
            openApi.put(OpenApiConstant.SERVERS_FIELD, Collections.singletonList(server));
            return Mono.just(openApi);
        };
    }
}
