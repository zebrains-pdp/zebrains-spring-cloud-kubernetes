package ru.prestu.gateway.config.property;

public interface ServiceProperty {

    String getId();

    String getServiceName();

    String getUri();

    String getApiPath();
}
