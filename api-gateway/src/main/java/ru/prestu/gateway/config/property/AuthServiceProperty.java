package ru.prestu.gateway.config.property;

import jakarta.validation.constraints.NotBlank;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@FieldDefaults(level = AccessLevel.PRIVATE)
@ConfigurationProperties(prefix = "auth-service")
public class AuthServiceProperty implements ServiceProperty {

    @NotBlank
    String id;
    @NotBlank
    String serviceName;
    @NotBlank
    String uri;
    @NotBlank
    String apiPath;
}
