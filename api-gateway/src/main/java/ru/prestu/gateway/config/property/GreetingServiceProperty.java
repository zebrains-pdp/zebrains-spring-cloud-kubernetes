package ru.prestu.gateway.config.property;

import jakarta.validation.constraints.NotBlank;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@FieldDefaults(level = AccessLevel.PRIVATE)
@ConfigurationProperties(prefix = "greeting-service")
public class GreetingServiceProperty implements ServiceProperty {

    @NotBlank
    String id;
    @NotBlank
    String serviceName;
    @NotBlank
    String uri;
    @NotBlank
    String apiPath;
}
