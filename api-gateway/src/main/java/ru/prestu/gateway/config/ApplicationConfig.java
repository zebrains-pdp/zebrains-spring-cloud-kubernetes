package ru.prestu.gateway.config;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.reactive.WebClientCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.function.client.WebClient;
import ru.prestu.common.CommonConfig;
import ru.prestu.gateway.config.property.AuthServiceProperty;
import ru.prestu.gateway.config.property.GreetingServiceProperty;
import ru.prestu.gateway.config.property.ServiceProperty;
import ru.prestu.gateway.constant.BeanNames;
import ru.prestu.security.SecurityConfig;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Configuration
@Import({
        CommonConfig.class,
        SecurityConfig.class
})
@EnableConfigurationProperties({
        AuthServiceProperty.class,
        GreetingServiceProperty.class
})
public class ApplicationConfig {

    @Bean
    @LoadBalanced
    public WebClient.Builder webClientBuilder(ObjectProvider<WebClientCustomizer> customizerProvider) {
        WebClient.Builder builder = WebClient.builder();
        customizerProvider.orderedStream().forEach(customizer -> customizer.customize(builder));
        return builder;
    }

    @Bean(name = BeanNames.ID_TO_SERVICE_PROPERTIES_MAP)
    public Map<String, ServiceProperty> idToServicePropertiesMap(List<ServiceProperty> servicePropertiesList) {
        return servicePropertiesList.stream().collect(
                Collectors.toMap(
                        ServiceProperty::getId,
                        Function.identity()
                )
        );
    }
}
