package ru.prestu.gateway.util;

import lombok.experimental.UtilityClass;
import org.springframework.cloud.gateway.route.RouteDefinition;
import ru.prestu.gateway.constant.OpenApiConstant;

@UtilityClass
public class RouteDefinitionUtil {

    private final String PATH_KEY = "_genkey_0";

    public String getServicePath(RouteDefinition routeDefinition) {
        return getPredicatePath(routeDefinition).replace("/**", "");
    }

    public String getApiDocsPath(RouteDefinition routeDefinition) {
        return getPredicatePath(routeDefinition).replace("/**", OpenApiConstant.API_PATH);
    }

    private String getPredicatePath(RouteDefinition routeDefinition) {
        return routeDefinition.getPredicates().stream().findFirst()
                .map(predicate -> predicate.getArgs().get(PATH_KEY))
                .orElse("/");
    }
}
