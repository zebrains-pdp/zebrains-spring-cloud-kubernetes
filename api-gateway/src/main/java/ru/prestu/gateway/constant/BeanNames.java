package ru.prestu.gateway.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class BeanNames {

	public final String ID_TO_SERVICE_PROPERTIES_MAP = "idToServicePropertiesMap";
}
