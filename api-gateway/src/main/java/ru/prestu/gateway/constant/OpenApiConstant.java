package ru.prestu.gateway.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class OpenApiConstant {

	public final String API_PATH = "/v3/api-docs";
	public final String SERVERS_FIELD = "servers";
}
